package im.djm.demotest.repository;

import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class GreetingRepository {

    public Map<String, String> greeting(String name) {
        Map<String, String> map = new HashMap<>();
        map.put("hello", name);

        return map;
    }

}
